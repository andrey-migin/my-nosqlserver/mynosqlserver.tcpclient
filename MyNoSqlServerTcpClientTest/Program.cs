﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using MyNoSqlServer.TcpClient;
using MyNoSqlServer.TcpClient.ReadRepository;

namespace MyNoSqlServerTcpClientTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var writer = new MyNoSqlServerClient<TestEntity>(()=>"http://localhost:5123", "testtable");

            var readerClient = new MyNoSqlTcpClient(()=>"127.0.0.1:5125", "test-app");
            
            
            var reader = new MyNoSqlReadRepository<TestEntity>(readerClient, "testtable");
            
            reader.SubscribeToChanges(itm =>
            {
                foreach (var changed in itm)
                {
                    Console.WriteLine("Got change: "+changed.MyField);
                }
            });
            
            readerClient.Start();

            var i = 0;
            while (true)
            {
                var entity = new TestEntity
                {
                    PartitionKey = "1",
                    RowKey = "1",
                    MyField = i.ToString()
                };

                writer.InsertOrReplaceAsync(entity).Wait();
                Console.WriteLine("Written:"+i);
                
                i++;

                Thread.Sleep(1000);  
                var readEntity = reader.Get().First();
                Console.WriteLine("Read: "+readEntity.MyField);
                Thread.Sleep(3000);
            }
   
        }
  
    }


    public class TestEntity : MyNoSqlTableEntity
    {
        public string MyField { get; set; }
    }
}